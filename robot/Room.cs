using System;

namespace Finit
{
    public struct Position {
        public int X;
        public int Y;
        
        public Position(int x, int y)
        {
            X = x;
            Y = y;
        }

        static public double DistanceBetween(Position p1, Position p2)
        {
            return Math.Sqrt(Math.Pow(p1.X - p2.X, 2) + Math.Pow(p1.Y - p2.Y, 2));
        }
    };

    public enum Orientation {
        West,
        North,
        East,
        South
    };

    public struct Bomb {
        public Position Position;
        public double SafeDistance;
        public string Code;
        public bool Defused;

        public Bomb(Position position, double safeDistance, string code)
        {
            Position = position;
            SafeDistance = safeDistance;
            Code = code;
            Defused = false;
        }
    };

    public struct Beacon {
        public Position Position;
        public int Code;

        public Beacon(Position position, int code) {
            Position = position;
            Code = code;
        }
    };

    public class Room
    {
        public uint SizeX, SizeY;
        private int[] Positions;
        private int FreePositions;
        private Random Random = new Random();
        public int CodebreakAttemptsMax;
        public int CodebreakAttempts { get; private set; }
        private bool TriedBreakCodeThisTurn = false;

        private Position InitialRobotPosition;
        private Orientation InitialRobotOrientation;
        public Position RobotPosition;
        public Orientation RobotOrientation;
        public Bomb Bomb;
        public Beacon[] Beacons;

        private void InitFreePositions()
        {
            Positions = new int[SizeX * SizeY];
            FreePositions = (int) (SizeX * SizeY);
            for (int i = 0; i < SizeX * SizeY; i++) {
                Positions[i] = i;
            }
        }

        private Position GetRandomPosition()
        {
            if (FreePositions <= 0) {
                throw new Exception("Sorry, the store is out of unique positions!");
            }
            
            int idx = Random.Next(FreePositions);
            int p = Positions[idx];
            int tmp = Positions[FreePositions - 1];
            Positions[FreePositions - 1] = p;
            Positions[idx] = tmp;
            FreePositions -= 1;

            // Fix corner cases when %'ing by 1
            return new Position(SizeX == 1 ? 0 : SizeY == 1 ? (int) (p % SizeX) : (int) (p / SizeX), (int) (p % SizeY));
        }

        private long Factorial(int n)
        {
            if (n == 1) {
                return n;
            }

            return Factorial((short) (n - 1)) * n;
        }

        private int GetMaxCodebreakAttempts(int numBeacons)
        {
            // Number of {beacons} digits permutations rounded up to a multiple of 5
            return 5 * (int) (Math.Round((Factorial(numBeacons) + 4.0) / 5.0));
        }

        public Room(uint sizeX, uint sizeY, short beacons)
        {
            if (beacons <= 0) {
                throw new Exception("Must have at least 1 beacon");
            }

            SizeX = sizeX;
            SizeY = sizeY;
            InitFreePositions();

            string code = Random.Next((int) Math.Pow(10, beacons)).ToString(String.Format("D{0}", beacons));
            CodebreakAttemptsMax = GetMaxCodebreakAttempts(beacons);

            Beacons = new Beacon[beacons];
            for (int i = 0; i < beacons; i++) {
                Beacons[i] = new Beacon(GetRandomPosition(), (int) Char.GetNumericValue(code, i));
            }

            InitialRobotPosition = GetRandomPosition();
            RobotPosition = InitialRobotPosition;
            InitialRobotOrientation = Orientation.South;
            RobotOrientation = InitialRobotOrientation;

            Bomb = new Bomb(GetRandomPosition(), 10.0, code);
        }

        public void Reset()
        {
            RobotPosition = InitialRobotPosition;
            RobotOrientation = InitialRobotOrientation;
            CodebreakAttemptsMax = GetMaxCodebreakAttempts(Beacons.Length);
            CodebreakAttempts = 0;
            Bomb.Defused = false;
        }

        public bool IsPositionValid(Position p)
        {
            return (p.X >= 0 && p.X < SizeX && p.Y >= 0 && p.Y < SizeY);
        }

        private Position AdvancePosition(Position p, Orientation o)
        {
            Position result = p;

            switch (o) {
                case Orientation.East:
                    result.X += 1;
                    break;
                case Orientation.West:
                    result.X -= 1;
                    break;
                case Orientation.South:
                    result.Y += 1;
                    break;
                case Orientation.North:
                    result.Y -= 1;
                    break;
            }

            return result;
        }

        public bool AdvanceRobot()
        {
            Position p = AdvancePosition(RobotPosition, RobotOrientation);
            if (IsPositionValid (p)) {
                RobotPosition = p;
                return true;
            }

            return false;
        }

        public void TurnRobotLeft()
        {
            int orientation = (int) RobotOrientation - 1;
            if (orientation < 0) {
                RobotOrientation = Orientation.South;
            } else {
                RobotOrientation = (Orientation) orientation;
            }
        }

        public void TurnRobotRight()
        {
            int orientation = (int) RobotOrientation + 1;
            if (orientation > 3) {
                RobotOrientation = Orientation.West;
            } else {
                RobotOrientation = (Orientation) orientation;
            }
        }

        public double GetRadiationLevel(Position p)
        {
            return 1.0 - Math.Min(Bomb.SafeDistance, Position.DistanceBetween(Bomb.Position, p)) / Bomb.SafeDistance;
        }

        public double GetIlluminationLevel()
        {
            Beacon? b = FindBeaconInFrontOfRobot();
            if (!b.HasValue) {
                return 0.0;
            }

            double maxDistance = Math.Max(SizeX, SizeY);
            return Math.Max(0.1, 1.0 - Position.DistanceBetween(RobotPosition, b.Value.Position) / maxDistance);
        }

        public double GetCodeSignal()
        {
            Beacon? b = FindBeaconAt(RobotPosition);
            if (!b.HasValue) {
                return Double.PositiveInfinity;
            }

            return b.Value.Code / 10.0 + Random.Next(100) * 0.001;
        }

        public bool IsRobotFacingWall()
        {
            return !IsPositionValid(AdvancePosition(RobotPosition, RobotOrientation));
        }

        public Beacon? FindBeaconAt(Position p)
        {
            Beacon? result = null;
            
            foreach (var b in Beacons) {
                if (p.X == b.Position.X && p.Y == b.Position.Y) {
                    result = b;
                    break;
                }
            }
            
            return result;
        }

        public bool IsBeaconAt(Position p)
        {
            if (FindBeaconAt(p) != null) {
                return true;
            }

            return false;
        }

        public bool IsBombAt(Position p)
        {
            return p.X == Bomb.Position.X && p.Y == Bomb.Position.Y;
        }

        public bool IsRobotLookingAtBeacon()
        {
            Position p = AdvancePosition(RobotPosition, RobotOrientation);
            while (IsPositionValid(p)) {
                if (IsBeaconAt(p)) {
                    return true;
                }
                p = AdvancePosition(p, RobotOrientation);
            }

            return false;
        }

        public Beacon? FindBeaconInFrontOfRobot()
        {
            Position p = AdvancePosition(RobotPosition, RobotOrientation);
            Beacon? b;

            while (IsPositionValid(p)) {
                b = FindBeaconAt(p);
                if (b != null) {
                    return b;
                }
                p = AdvancePosition(p, RobotOrientation);
            }
            
            return null;
        }

        public void NextTurn()
        {
            TriedBreakCodeThisTurn = false;
        }

        public void TryBreakCode(string code)
        {
            if (!IsBombAt(RobotPosition)) {
                throw new Exception("You must find the bomb first!");
            }

            if (!HasCodebreakAttempts()) {
                throw new Exception("Out of codebreak attempts");
            }

            if (TriedBreakCodeThisTurn) {
                throw new Exception("Already attempted to break code this turn");
            }

            if (code == Bomb.Code) {
                Bomb.Defused = true;
            }

            CodebreakAttempts += 1;
            TriedBreakCodeThisTurn = true;
        }

        public bool HasCodebreakAttempts()
        {
            return (CodebreakAttempts < CodebreakAttemptsMax);
        }
    }
}


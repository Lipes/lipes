using System;
using Finit;

namespace Finit.Robot
{
    public class Sensor
    {
        public delegate double SensorValueFunc();
        private SensorValueFunc ValueFunc;

        public double Value {
            get {
                return ValueFunc();
            }
        }

        public Sensor(SensorValueFunc valueFunc)
        {
            this.ValueFunc = valueFunc;
        }
    }
}

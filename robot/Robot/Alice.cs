using System;
using Finit.Robot;
using System.Collections.Generic;

namespace Finit.Robot
{
    public class Alice : IRobot
    {
        public void DoTurn(Hardware hardware)
        {
            // Read sensors like this:
            // hardware.sensors["radiation"].Value

            // Break bomb code like this:
            // hardware.codebreaker.Operate("1953");

            // Give movement commands:
            hardware.NextCommand = Command.Noop;
        }

        public void Reboot()
        {
            // Reset state
            // Prepare for new mission
        }
    }
}
